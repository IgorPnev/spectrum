import { buildFavicon, buildInjectFavicon } from "./tools/build-favicon"
import { dest, lastRun, parallel, series, src, watch } from "gulp"

import { create as bsCreate } from "browser-sync"
import { buildCss } from "./tools/build-css"
import { buildFiles } from "./tools/build-files"
import { buildImage } from "./tools/build-image"
import { buildPug } from "./tools/build-pug"
import { copyFavicon } from "./tools/copy-favicon"
import { buildSvg, buildSvgBitrix } from "./tools/build-svg"
import { buildFonts } from "./tools/build-fonts"
import { sync } from "./tools/build-rsync"

import config from "./webpack.config"
import stripAnsi from "strip-ansi"
import webpack from "webpack"
import webpackDevMiddleware from "webpack-dev-middleware"
import { replaceManifest } from "./tools/build-revision"

const browserSync = bsCreate()
const reload = browserSync.reload

const dirs = {
  src: "src",
  dest: "build"
}

const sources = {
  pug: `${dirs.src}/template/**/*.pug`,
  cssLayout: `${dirs.src}/css/layout/**/*.css`,
  cssUi: `${dirs.src}/css/ui/**/*.css`,
  css: `${dirs.src}/css/**/*.css`
}

const bundler = webpack(config)

bundler.plugin("done", function(stats) {
  if (stats.hasErrors() || stats.hasWarnings()) {
    return browserSync.sockets.emit("fullscreen:message", {
      title: "Webpack Error:",
      body: stripAnsi(stats.toString()),
      timeout: 100000
    })
  }
  browserSync.reload()
})

export const browserSyncStart = () => {
  browserSync.init({
    files: ["build/**/*"],
    server: {
      baseDir: dirs.dest,
      directory: true,
      middleware: [
        webpackDevMiddleware(
          bundler,
          {
            publicPath: "/js",
            noInfo: false,
            quiet: false,
            reload: true,
            index: "index.html",
            stats: {
              assets: true,
              colors: true,
              version: false,
              hash: false,
              timings: true,
              chunks: false,
              chunkModules: false
            }
          },
          browserSync.reload()
        )
      ]
    }
  })
}

export const browserSyncProxy = () => {
  browserSync.init({
    proxy: "https://exdeniz.ilimtimber.danke.agency/"
  })
}

export const browserSyncStream2 = () => {
  return src("../local/**/main.css", {
    dot: true
  }).pipe(
    browserSync.reload({
      stream: true
    })
  )
}

export const browserSyncStream = done => {
  browserSync.reload({ stream: true })
  done()
}

export const browserSyncReload = done => {
  browserSync.reload()
  done()
}

export const devWatch = () => {
  global.watch = true
  watch([sources.pug, "./build/img/sprite.svg"], series(buildPug)).on("all", (event, filepath) => {
    global.changedTemplateFile = filepath.replace(/\\/g, "/")
  })
  watch(sources.css, series(buildCss))
  // watch("src/svg/**/*.svg", series(buildSvg))
  watch("./src/svg/*.svg", series(buildSvgBitrix, replaceManifest))
  watch("./src/img/**/*.{webp,png,jpg,jpeg}", series(buildImage))
  watch("./src/fonts/**/*.{woff,woff2}", series(buildFonts))
}

export const proxyWatch = () => {
  global.watch = true
  watch(sources.css, series(buildCss, sync, browserSyncReload))
  watch("./src/svg/*.svg", series(buildSvgBitrix, replaceManifest))
  watch("../local/**/*..{webp,png,jpg,jpeg,svg}", series(browserSyncReload))
  watch("./src/fonts/**/*.{woff,woff2}", series(buildFonts))
  watch(
    "../local/**/*.{php,js}",
    {
      dot: true
    },
    series(sync, browserSyncReload)
  )
}

const buildHtml = series(buildFavicon, copyFavicon, buildPug)

export const build = parallel(buildCss, buildSvgBitrix, buildHtml, buildImage, buildFonts, buildFiles)
export const buildprod = parallel(buildCss, buildSvgBitrix)

export const dev = parallel(devWatch, browserSyncStart)
export const proxy = parallel(proxyWatch, browserSyncProxy)

export default dev
