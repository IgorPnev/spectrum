/* eslint-disable no-unused-vars */

module.exports = ctx => ({
  plugins: {
    "postcss-import-ext-glob": {},
    "postcss-import": {},
    "postcss-preset-env": {
      stage: 0
    },
    "postcss-flexbox": {},
    lost: {},
    "postcss-center": {},
    "postcss-css-reset": {},
    autoprefixer: {},
    cssnano: ctx.env === "production" ? { preset: "advanced" } : false,
    "postcss-reporter": {}
  }
})
