'use strict'

const webpack = require('webpack')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const CleanWebpackPlugin = require('clean-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const path = require('path')
module.exports = {
  devtool: 'source-map',
  performance: { hints: false },
  watch: true,
  mode: 'development',
  entry: {
    // polyfill: "./src/js/polyfill.js",
    app: './src/js/app.js'
  },
  output: {
    path: __dirname + '/build/js',
    // path: __dirname + "./../local/templates/ilim/js",
    chunkFilename: '[name].js',
    publicPath: '/js',
    filename: '[name].js'
    // libraryTarget: "window",
    // library: "[name]"
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,

        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/env',
                  {
                    targets: {
                      esmodules: true
                      // browsers: ["Chrome >= 60", "Safari >= 10.1", "iOS >= 10.3", "Firefox >= 54", "Edge >= 15"]
                    },
                    useBuiltIns: 'usage'
                  }
                ],
                '@babel/preset-react'
              ],
              plugins: [
                '@babel/plugin-proposal-object-rest-spread',
                // "syntax-async-functions",
                '@babel/plugin-proposal-class-properties',
                // "transform-custom-element-classes",
                '@babel/plugin-transform-regenerator'
              ]
            }
          }
        ]
      }
    ]
  },
  resolve: {
    alias: {
      helper: path.resolve(__dirname, 'src/js/helper'),
      component: path.resolve(__dirname, 'src/js/component')
    }
  },
  optimization: {
    splitChunks: {
      chunks: 'initial',
      maxInitialRequests: 1,
      minSize: 0,
      name: false
    }
  },
  plugins: []
}

if (process.env.NODE_ENV === 'legacy') {
  module.exports.devtool = false
  module.exports.watch = false
  module.exports.mode = 'production'
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    })
    // new BundleAnalyzerPlugin()
  ])

  module.exports.entry = {
    app: './src/js/app.js'
  }

  module.exports.output = {
    path: __dirname + './../local/templates/kdsi/js',
    chunkFilename: '[name].es5.js',
    filename: '[name].es5.js',
    libraryTarget: 'window',
    library: '[name]'
  }
  module.exports.module.rules = [
    {
      test: /\.m?js$/,
      // exclude: /node_modules/,

      use: [
        {
          loader: 'babel-loader',
          options: {
            presets: [
              [
                '@babel/env',
                {
                  useBuiltIns: 'entry',
                  targets: {
                    browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'ie 11']
                  }
                }
              ],
              '@babel/preset-react'
            ],
            plugins: [
              '@babel/plugin-proposal-object-rest-spread',
              '@babel/plugin-proposal-class-properties',
              '@babel/plugin-transform-regenerator'
            ]
          }
        }
      ]
    }
  ]

  module.exports.optimization.minimizer = [
    new TerserPlugin({
      parallel: true,
      sourceMap: true,
      terserOptions: {
        ecma: undefined,
        warnings: false,
        parse: {},
        compress: {},
        mangle: true, // Note `mangle.properties` is `false` by default.
        module: false,
        output: null,
        toplevel: false,
        nameCache: null,
        ie8: false,
        keep_classnames: undefined,
        keep_fnames: false,
        safari10: true
      }
    })
  ]
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = false
  module.exports.watch = false
  module.exports.mode = 'production'
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    })
    // new BundleAnalyzerPlugin()
  ])
  module.exports.optimization.minimizer = [
    new TerserPlugin({
      parallel: true,
      sourceMap: true,
      terserOptions: {
        ecma: undefined,
        warnings: false,
        parse: {},
        compress: {},
        mangle: true, // Note `mangle.properties` is `false` by default.
        module: false,
        output: null,
        toplevel: false,
        nameCache: null,
        ie8: false,
        keep_classnames: undefined,
        keep_fnames: false,
        safari10: true
      }
    })
  ]
}

if (process.env.NODE_ENV === 'local') {
  module.exports.output.path = __dirname + '/build/js'
}
