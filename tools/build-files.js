import { dest, lastRun, parallel, series, src, watch } from 'gulp'

export const buildFiles = () => src('./src/files/**/*').pipe(dest('./build/files'))
