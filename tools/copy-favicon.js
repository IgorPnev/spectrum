import { dest, lastRun, parallel, series, src, watch } from 'gulp'

export const copyFavicon = () => src('./build/favicon/favicon.ico')
    .pipe(dest('./build/'))
