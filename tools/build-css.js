import { dest, lastRun, parallel, series, src, watch } from "gulp"

import plumber from "gulp-plumber"
import postcss from "gulp-postcss"
import sourcemaps from "gulp-sourcemaps"
import util from "gulp-util"

const production = !!util.env.production

const dirs = {
  src: "src",
  dest: "build"
}

const sources = {
  css: `${dirs.src}/css/main.css`
}

export const buildCss = () =>
  src([sources.css])
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(postcss()) // Конфигурация берется из файла postcss.config.js
    .pipe(sourcemaps.write("./"))
    .pipe(dest(dirs.dest + "/css/"))
    .pipe(dest("../local/templates/ilim/css/"))
