import rsync from "gulp-rsync"
import { src } from "gulp"
import rsyncwrapper from "rsyncwrapper"

const rsyncOptions = {
  login: "exdeniz",
  address: "178.62.217.8",
  path: "/var/www/exdeniz.ilimtimber.danke.agency",
  dest: "exdeniz@178.62.217.8:/var/www/exdeniz.ilimtimber.danke.agency"
}
export const sync = done => {
  rsyncwrapper(
    {
      src: "../",
      // username: rsyncOptions.login,
      dest: rsyncOptions.dest,
      // hostname: rsyncOptions.address,
      ssh: true,
      recursive: true,
      times: true,
      args: ["--verbose", "-h", "--update"],
      exclude: [
        "/.git",
        ".vscode",
        "/html/node_modules",
        "/html/gulp/node_modules",
        "tmp",
        "/vendor",
        "/design",
        "/bitrix",
        "/upload",
        "/thumb",
        "/auth",
        "/verstka",
        "/html/parcel",
        "/html/gulp",
        "/html/gulp old"
      ],
      include: [
        "/local/templates/ilim/css/main.css",
        "/local/templates/ilim/css/main.css.map",
        "/local/templates/ilim/js/app.js",
        "/local/templates/ilim/js/app.js.map"
      ]
    },
    function(error, stdout, stderr, cmd) {
      if (error) {
        // failed
        console.log(error.message)
      } else {
        console.log(stdout)
        done()
      }
    }
  )
}

//  return gulp.src('../local/**/*.*', { dot: true })
// export const sync = () =>
//   src(["../**/*.*", "!../html/**/*.*", "!../bitrix/**/*.*", "!../verstka/**/*.*", "!../.git/**/*.*"], {
//     dot: true
//   }).pipe(
//     rsync({
//       root: "../",
//       username: rsyncOptions.login,
//       destination: rsyncOptions.path,
//       hostname: rsyncOptions.address,
//       recursive: true,
//       verbose: true,
//       progress: true,
//       incremental: true,
//       archive: false,
//       compress: true,
//       update: false,
//       exclude: [
//         ".sync-config.cson",
//         ".git",
//         ".vscode",
//         "node_modules",
//         "tmp",
//         "vendor",
//         "design",
//         "bitrix",
//         "upload",
//         "thumb",
//         "auth"
//       ]
//     })
//   )
