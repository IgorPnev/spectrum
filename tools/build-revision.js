import { src, dest, watch, parallel, series, lastRun } from "gulp"
import revRewrite from "gulp-rev-rewrite"
import rename from "gulp-rename"
import replace from "gulp-replace-string"
export const replaceManifest = () => {
  const manifest = src("../local/templates/kdsi/svg/rev-manifest.json")
  return src("./src/js/svg-loader-template.js")
    .pipe(replace("@src@", "/local/templates/kdsi/svg/sprite.svg"))
    .pipe(revRewrite({ manifest }))
    .pipe(rename("svg-loader.js"))
    .pipe(dest("../local/templates/kdsi/js/"))
    .pipe(replace("/local/templates/kdsi/", "./"))
    .pipe(dest("./build/js/"))
}
