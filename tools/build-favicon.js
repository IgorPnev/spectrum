import { dest, lastRun, parallel, series, src, watch } from "gulp"

import realFavicon from "gulp-real-favicon"

const FAVICON_DATA_FILE = "faviconData.json"

export const buildFavicon = done => {
  realFavicon.generateFavicon(
    {
      masterPicture: "src/favicon/favicon_src.png",
      dest: "build/favicon/",
      iconsPath: "/favicon/",
      design: {
        ios: {
          pictureAspect: "noChange",
          assets: {
            ios6AndPriorIcons: false,
            ios7AndLaterIcons: false,
            precomposedIcons: false,
            declareOnlyDefaultIcon: true
          }
        },
        desktopBrowser: {},
        windows: {
          pictureAspect: "noChange",
          backgroundColor: "#da532c",
          onConflict: "override",
          assets: {
            windows80Ie10Tile: false,
            windows10Ie11EdgeTiles: {
              small: false,
              medium: true,
              big: false,
              rectangle: false
            }
          }
        },
        androidChrome: {
          pictureAspect: "noChange",
          themeColor: "#ffffff",
          manifest: {
            display: "standalone",
            orientation: "notSet",
            onConflict: "override",
            declared: true
          },
          assets: {
            legacyIcon: false,
            lowResolutionIcons: false
          }
        },
        safariPinnedTab: {
          pictureAspect: "silhouette",
          themeColor: "#5bbad5"
        }
      },
      settings: {
        scalingAlgorithm: "Mitchell",
        errorOnImageTooSmall: false
      },
      markupFile: FAVICON_DATA_FILE
    },
    function() {
      done()
    }
  )
}
