import { q, qq } from "../../helper/q"

export const casesInit = () => {
  const expandElement = qq(".casesItem")
  expandElement.map(item => {
    q(".casesItemHeader", item).addEventListener("click", () => {
      item.classList.toggle("casesItemClose")
    })
  })
}
