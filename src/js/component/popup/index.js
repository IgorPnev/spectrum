import tingle from 'tingle.js'
import { q, qq } from '../../helper/q'
import Glide, { Controls, Swipe, Autoplay } from '@glidejs/glide/dist/glide.modular.esm'

export const popupInit = () => {
  const modal = new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ['overlay', 'escape'],
    onOpen: () => {
      q('.popupClose', modal.getContent()).addEventListener('click', () => {
        modal.close()
      })
      initSlider()
    }
  })

  qq('.popupLink').map(button => {
    button.addEventListener('click', e => {
      fetch(button.dataset.popup)
        .then(html => html.text())
        .then(html => {
          modal.setContent(html)
          modal.open()
        })
    })
  })

  window.addEventListener('load', () => {
    setTimeout(() => {
      fetch(popupload.url)
        .then(html => html.text())
        .then(html => {
          modal.setContent(html)
          modal.open()
        })
    }, popupload.timeout)
  })
}

const initSlider = () => {
  if (!q('.popupSlider')) return null

  const popupSlider = new Glide('.popupSlider', {
    type: 'slider',
    startAt: 0,
    perView: 1,
    gap: 0
    // bound: true
    // autoplay: 5000
  }).mount({
    Controls,
    Swipe,
    Autoplay
  })
  setTimeout(() => {
    window.dispatchEvent(new Event('resize'))
  }, 100)
}
