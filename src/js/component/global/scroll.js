import SmoothScroll from 'smooth-scroll'
import Gumshoe from 'gumshoejs'
import { q } from 'helper/q'
export let scroll

export const scrollInit = () => {
  scroll = new SmoothScroll('a[href*="#"]', {
    speed: 300,
    // header: ".header",
    offset: q('.header').offsetHeight
  })

  let spy = new Gumshoe('.headerMenu a', {
    nested: true,
    nestedClass: 'active',
    reflow: true,
    offset: 180
  })
}
