import { q, qq } from '../../helper/q'

export const contentInit = () => {
  const expandElement = qq('.contentItem')
  expandElement.map(item => {
    q('.contentItemButton', item).addEventListener('click', () => {
      item.classList.add('contentItemOpen')
    })
    q('.contentItemMoreClose', item).addEventListener('click', () => {
      item.classList.remove('contentItemOpen')
    })
  })
}
