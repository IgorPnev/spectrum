/*global google contactMap*/
import { q, qq } from '../helper/q'

export const mapInit = () => {
  const stylesMap = [
    {
      featureType: 'all',
      elementType: 'geometry.fill',
      stylers: [
        {
          weight: '2.00'
        }
      ]
    },
    {
      featureType: 'all',
      elementType: 'geometry.stroke',
      stylers: [
        {
          color: '#9c9c9c'
        }
      ]
    },
    {
      featureType: 'all',
      elementType: 'labels.text',
      stylers: [
        {
          visibility: 'on'
        }
      ]
    },
    {
      featureType: 'landscape',
      elementType: 'all',
      stylers: [
        {
          color: '#f2f2f2'
        }
      ]
    },
    {
      featureType: 'landscape',
      elementType: 'geometry.fill',
      stylers: [
        {
          color: '#ffffff'
        }
      ]
    },
    {
      featureType: 'landscape.man_made',
      elementType: 'geometry.fill',
      stylers: [
        {
          color: '#ffffff'
        }
      ]
    },
    {
      featureType: 'poi',
      elementType: 'all',
      stylers: [
        {
          visibility: 'off'
        }
      ]
    },
    {
      featureType: 'road',
      elementType: 'all',
      stylers: [
        {
          saturation: -100
        },
        {
          lightness: 45
        }
      ]
    },
    {
      featureType: 'road',
      elementType: 'geometry.fill',
      stylers: [
        {
          color: '#eeeeee'
        }
      ]
    },
    {
      featureType: 'road',
      elementType: 'labels.text.fill',
      stylers: [
        {
          color: '#7b7b7b'
        }
      ]
    },
    {
      featureType: 'road',
      elementType: 'labels.text.stroke',
      stylers: [
        {
          color: '#ffffff'
        }
      ]
    },
    {
      featureType: 'road.highway',
      elementType: 'all',
      stylers: [
        {
          visibility: 'simplified'
        }
      ]
    },
    {
      featureType: 'road.arterial',
      elementType: 'labels.icon',
      stylers: [
        {
          visibility: 'off'
        }
      ]
    },
    {
      featureType: 'transit',
      elementType: 'all',
      stylers: [
        {
          visibility: 'off'
        }
      ]
    },
    {
      featureType: 'water',
      elementType: 'all',
      stylers: [
        {
          color: '#46bcec'
        },
        {
          visibility: 'on'
        }
      ]
    },
    {
      featureType: 'water',
      elementType: 'geometry.fill',
      stylers: [
        {
          color: '#c8d7d4'
        }
      ]
    },
    {
      featureType: 'water',
      elementType: 'labels.text.fill',
      stylers: [
        {
          color: '#070707'
        }
      ]
    },
    {
      featureType: 'water',
      elementType: 'labels.text.stroke',
      stylers: [
        {
          color: '#ffffff'
        }
      ]
    }
  ]

  const mapElement = document.getElementById('map')
  if (mapElement) {
    const mapStores = new google.maps.Map(mapElement, {
      scrollwheel: false,
      zoom: 17,
      minZoom: 2,
      center: new google.maps.LatLng(55.755825, 37.617298),
      streetViewControl: false,
      mapTypeControl: false,
      styles: stylesMap,
      scaleControl: false,
      zoomControl: true,
      fullscreenControl: false
    })

    const bounds = new google.maps.LatLngBounds()
    const markers = {}

    contactMap.map((item, index) => {
      markers[index] = new google.maps.Marker({
        icon: {
          url: '/img/LOGO_SIGN.svg',
          size: new google.maps.Size(56, 66),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(28, 66),
          scaledSize: new google.maps.Size(56, 66),
          labelOrigin: new google.maps.Point(56, 66)
        },
        position: new google.maps.LatLng(item.latitude, item.longitude),
        map: mapStores,
        optimized: false

        // label: { fontFamily: "lato", text: item.companyInfo.name, color: "white" }
      })

      bounds.extend(new google.maps.LatLng(item.latitude, item.longitude))

      markers[index].addListener('click', () => {
        mapStores.setZoom(16)
        mapStores.setCenter(markers[index].getPosition())
      })
    })

    mapStores.setCenter(markers[0].getPosition())
    mapStores.setZoom(16)
    // mapStores.fitBounds(bounds)

    // window.addEventListener('resize', () => {
    //   mapStores.setCenter(bounds.getCenter())
    //   mapStores.fitBounds(bounds)
    //   mapStores.setZoom(mapStores.getZoom() - 1)
    // })

    const buttonsToZoom = qq('.contactItemHeader')
    buttonsToZoom.map((button, index) => {
      button.addEventListener('click', () => {
        setTimeout(() => {
          mapStores.setZoom(16)
          mapStores.setCenter(markers[index].getPosition())
        }, 500)
      })
    })
  }
}
