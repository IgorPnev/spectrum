import { sliderObserver } from '../index'
import { q, qq } from '../../../helper/q'
import pose from 'popmotion-pose'

let slide = {}

const textProps = {
  initialPose: 'stop',
  start: {
    delay: 0,
    delayChildren: 200,
    staggerChildren: 150
  },
  stop: {
    delayChildren: 50,
    staggerChildren: 50
  }
}

const textVerticalProps = {
  initialPose: 'stop',
  start: {
    opacity: 1,
    y: 0,
    transition: { duration: 600 }
  },
  stop: {
    opacity: 0,
    y: 60,
    transition: { duration: 200 }
  }
}

const textHorizontalProps = {
  initialPose: 'stop',
  start: {
    opacity: 1,
    x: 0,
    transition: { duration: 600 }
  },
  stop: {
    opacity: 0,
    x: -300,
    transition: { duration: 200 }
  }
}

const photoProps = {
  initialPose: 'stop',
  start: {
    opacity: 1,
    x: 0,
    transition: { duration: 600 }
  },
  stop: {
    opacity: 0,
    x: '100%',
    transition: { duration: 200 }
  }
}

export const one = slideElement => {
  if (!slideElement) return null

  let items = []

  const text = q('.slideWrap', slideElement)
  const bg = q('.slidePhoto', slideElement)

  const textPoser = pose(text, textProps)
  textPoser.addChild(bg, photoProps)
  // textPoser.addChild(q(".slideHeader", text), textHorizontalProps)
  // textPoser.addChild(q(".slideSubheader", text), textVerticalProps)

  sliderObserver.subscribe(event => {
    slide = event.active
    const action = event.action
    if (slideElement) {
      textPoser.set(action)
    }
  })
}
