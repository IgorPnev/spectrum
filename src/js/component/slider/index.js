import Glide, { Controls, Swipe, Autoplay } from '@glidejs/glide/dist/glide.modular.esm'
import { q, qq } from '../../helper/q'
import { EventObserver } from '../../helper/eventObserver'
import { one } from './slides/one'
import { textAnimate } from './animate-text'

export const sliderObserver = new EventObserver()

export const sliderInit = () => {
  if (!q('.sliderMain')) return null
  qq('.slide').map(el => {
    one(el)
  })
  // one(q('.slide-one'))
  // one(q('.slide-two'))
  // one(q('.slide-three'))

  const slider = new Glide('.sliderMain', {
    type: 'slider',
    startAt: 0,
    perView: 1,
    gap: 0,
    bound: true,
    autoplay: 5000
  })

  slider.on('mount.after', () => {
    sliderObserver.broadcast({
      active: slider._c.Html.slides[0],
      action: 'start'
    })
    textAnimate()
  })

  slider.on(['run.after'], () => {
    sliderObserver.broadcast({
      active: slider._c.Html.slides[slider.index],
      action: 'start'
    })
  })
  slider.on(['run.after'], () => {
    sliderObserver.broadcast({
      active: slider._c.Html.slides[slider.index],
      action: 'start'
    })
  })

  slider.on(['run.before'], () => {
    sliderObserver.broadcast({
      active: slider._c.Html.slides[slider.index],
      action: 'stop'
    })
  })

  slider.mount({
    Controls,
    Swipe,
    Autoplay
  })
  setTimeout(() => {
    window.dispatchEvent(new Event('resize'))
  }, 100)
}
