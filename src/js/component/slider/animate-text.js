import { q, qq } from '../../helper/q'
import pose from 'popmotion-pose'

let slide = {}

const textProps = {
  initialPose: 'stop',
  start: {
    delay: 0,
    delayChildren: 200,
    staggerChildren: 150
  },
  stop: {
    delayChildren: 50,
    staggerChildren: 50
  }
}

const textVerticalProps = {
  initialPose: 'stop',
  start: {
    opacity: 1,
    y: 0,
    transition: { duration: 600 }
  },
  stop: {
    opacity: 0,
    y: 60,
    transition: { duration: 200 }
  }
}

const textHorizontalProps = {
  initialPose: 'stop',
  start: {
    opacity: 1,
    x: 0,
    transition: { duration: 600 }
  },
  stop: {
    opacity: 0,
    x: -300,
    transition: { duration: 200 }
  }
}

const photoProps = {
  initialPose: 'stop',
  start: {
    opacity: 1,
    x: 0,
    transition: { duration: 600 }
  },
  stop: {
    opacity: 0,
    x: '100%',
    transition: { duration: 200 }
  }
}

export const textAnimate = () => {
  const textElement = q('.slideContent')
  if (!textElement) return null

  let items = []

  const textPoser = pose(textElement, textProps)

  textPoser.addChild(q('.slideHeader', textElement), textHorizontalProps)
  textPoser.addChild(q('.slideSubheader', textElement), textVerticalProps)
  textPoser.set('start')
}
