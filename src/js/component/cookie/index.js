import { q, qq } from '../../helper/q'
import pose from 'popmotion-pose'
import Cookies from 'js-cookie'

const cookieProps = {
  initialPose: 'stop',
  start: {
    opacity: 1,
    scale: 1,
    transition: { duration: 600 }
  },
  stop: {
    opacity: 0,
    scale: 0,
    transition: { duration: 200 }
  }
}

const cookieTextProps = {
  initialPose: 'stop',

  start: {
    opacity: 1,
    y: 0,
    transition: { duration: 600, delay: 600 }
  },
  stop: {
    opacity: 0,
    y: 60,
    transition: { duration: 200, delay: 600 }
  }
}

export const showCookie = () => {
  const cookieElement = q('.cookies')
  if (!cookieElement) return null
  if (Cookies.get('accept_cookie')) return null

  const cookiePosed = pose(cookieElement, cookieProps)
  const cookieTextPosed = pose(q('.cookiesWrap'), cookieTextProps)

  cookiePosed.set('start')
  cookieTextPosed.set('start')

  const button = q('.cookiesButton')
  if (!button) return null
  button.addEventListener('click', () => {
    cookiePosed.set('stop')
    cookieTextPosed.set('stop')
    Cookies.set('accept_cookie', true)
  })
}
