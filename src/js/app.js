import { mapInit } from './component/map'
import { scrollInit } from './component/global/scroll'
import { casesInit } from './component/cases/'
import { sliderInit } from './component/slider'
import ScrollOut from 'scroll-out'
import { popupInit } from './component/popup'
import { menuInit } from './component/menu'
import { showCookie } from './component/cookie'
import { contentInit } from './component/global/content'

document.addEventListener('DOMContentLoaded', () => {
  sliderInit()
  casesInit()
  scrollInit()
  mapInit()
  popupInit()
  menuInit()
  contentInit()
})

window.onload = function() {
  ScrollOut({
    once: true
    //offset: 400
  })
  showCookie()
}
